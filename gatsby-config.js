/* eslint-disable */
module.exports = {
  siteMetadata: {
    title: `Hugo Martinez | Lead Front End Developer`,
    siteUrl: `http://www.hugomartinez.tech`,
    description: `I'm Hugo Martinez and am currently based in Paris working as a 
    Lead Front End Developer on myCANAL. Here you will learn more about me and maybe about front
    end development in general`,
    author: `Hugo Martinez`
  },
  plugins: [
    `gatsby-plugin-preact`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/images`
      }
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-typescript`,
    `gatsby-plugin-sitemap`,
    `gatsby-plugin-netlify`,
    {
      resolve: "gatsby-plugin-transition-link",
      options: {
          layout: require.resolve(`${__dirname}/src/layouts/MainLayout/index.tsx`)
        }
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/assets/images/favicon.png` // This path is relative to the root of the site.
      }
    },
    {
      resolve: `gatsby-plugin-styled-components`,
      options: {
        // Add any options here
      }
    },
    {
      resolve: "gatsby-background-image",
      options: {
        // add your own characters to escape, replacing the default ':/'
        specialChars: "/:"
      }
    },
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: `${__dirname}/src/assets/images`, // // See below to configure properly
          omitKeys: ['id'],
        }
      }
    },
    {
      resolve: `gatsby-plugin-intl`,
      options: {
        path: `${__dirname}/src/intl`,
        languages: [`en`, `fr`],
        defaultLanguage: `fr`,
        redirect: false,
      },
    },
    {
      resolve: "gatsby-plugin-robots-txt",
      options: {
        host: "https://www.hugomartinez.tech",
        sitemap: "https://www.hugomartinez.tech/sitemap.xml",
        policy: [{ userAgent: "*", allow: "/" }]
      }
    },
    {
      resolve: `gatsby-source-gitlab`,
      options: {
        // You can get your access token on your GitLab profile
        accessToken: '_yUQTmbeEzbR9maXMFZ-',
      }
    },
    {
      resolve: `gatsby-source-medium`,
      options: {
        username: `@hugo.83300`,
      }
    },
    {
      resolve: `gatsby-source-github-api`,
      options: {
        token: "0ef97242df25f0ccbd4fd69029bde0a45d32944a",
        graphQLQuery: `
        query { 
          user(login: "hmartinezdev") {
            repositories(first: 3) {
              edges {
                node {
                  id
                  name
                  url
                  description
                  stargazers {
                    totalCount
                  }
                }
              }
            }
          }
        }`
      },
    }
  ]
};
