import React, { FC } from 'react';
import styled from 'styled-components';
import DivWithBorder from '../../styled/DivWithBorder';
import HolographicText, { Holographic } from '../../components/HolographicText';
import SocialMedias from '../../components/SocialMedias';
import ExternalLink from '../../styled/ExternalLink';
import { device } from '../../constants/mq';
import { useIntl } from 'gatsby-plugin-intl';

const Title = styled.h1`
  font-size: 50px;
  letter-spacing: 8px;
  font-family: ${(props) => props.theme.fonts.primary};
  color: ${(props) => props.theme.colors.pink};
  margin: 5px 0;
  text-align: center;

  @media ${device.mobileM} {
    font-size: 60px;
  }

  @media ${device.tablet} {
    font-size: 70px;
  }

  ${Holographic} {
    font-size: 20px;
    word-spacing: 5px;
    letter-spacing: initial;

    @media ${device.mobileM} {
      font-size: 24px;
    }

    @media ${device.tablet} {
      font-size: 27px;
    }
  }
`;

const TitleContainer = styled.div`
  display: flex;
  padding: 5px 10px;
  flex-direction: column;
`;

const CentralText = styled(DivWithBorder)`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 50px;
  width: 280px;
  height: 80px;
  font-weight: 700;
  z-index: 1;

  @media ${device.mobileM} {
    width: 360px;
    height: 80px;
  }

  @media ${device.tablet} {
    width: 420px;
    height: 100px;
  }

  & .CentralText__text {
    font-family: ${(props) => props.theme.fonts.secondary};
    text-align: center;
    color: ${(props) => props.theme.colors.main};
    font-size: 16px;
    margin: 0;

    @media ${device.mobileM} {
      font-size: 18px;
    }

    @media ${device.tablet} {
      font-size: 20px;
    }
  }

  & .CentralText__container {
    display: flex;
    align-items: center;
    height: 70px;
    width: 260px;
    background-color: #00000075;
    z-index: 2;

    @media ${device.mobileM} {
      height: 60px;
      width: 340px;
    }

    @media ${device.tablet} {
      height: 80px;
      width: 400px;
    }
  }
`;

const Container = styled.main`
  position: relative;
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
  justify-content: space-evenly;
  align-self: stretch;
  align-items: center;
  opacity: 0;
  transition: opacity 300ms ease-in-out;

  &.entered {
    opacity: 1;
  }

  &.entering {
    opacity: 0;
  }
`;

export interface HomeLayoutProps {
  transitionStatus: string;
}

const HomeLayout: FC<HomeLayoutProps> = ({ transitionStatus }) => {
  const intl = useIntl();
  return (
    <Container className={transitionStatus}>
      <TitleContainer>
        <Title>
          hugo martinez
          <HolographicText text={intl.formatMessage({ id: 'home.subtitle' })} />
        </Title>
      </TitleContainer>
      <CentralText>
        <div className="CentralText__container">
          <p className="CentralText__text">
            {intl.formatMessage({ id: 'home.mainText' })}
            <ExternalLink
              rel="noreferrer"
              href="https://www.canalplus.com"
              target="_blank">
              MYCANAL
            </ExternalLink>
          </p>
        </div>
      </CentralText>
      <SocialMedias />
    </Container>
  );
};

export default HomeLayout;
