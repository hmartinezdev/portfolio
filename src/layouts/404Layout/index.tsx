import React from 'react';
import styled from 'styled-components';
import SEO from '../../components/SEO';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  color: ${(props) => props.theme.colors.main};
  font-family: ${(props) => props.theme.fonts.primary};

  & > h1 {
    font-size: 60px;
  }

  & > p {
    font-size: 38px;
  }
`;

const Layout = (): JSX.Element => (
  <Container>
    <SEO title="404: Not found" />
    <h1>NOT FOUND</h1>
    <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
  </Container>
);

export default Layout;
