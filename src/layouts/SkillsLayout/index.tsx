import React, { FC } from 'react';
import styled from 'styled-components';
import Skill from '../../components/Skill';
import SkillHover from '../../components/SkillHover';
import ReactLogo from '../../assets/images/react-logo.svg';
import ReduxLogo from '../../assets/images/redux-logo.svg';
import TypeScriptLogo from '../../assets/images/typescript-logo.svg';
import DockerLogo from '../../assets/images/docker-logo.svg';
import JestLogo from '../../assets/images/jest-logo.svg';
import { device } from '../../constants/mq';

const Container = styled.main`
  display: flex;
  position: relative;
  padding: 60px 10px 10px 10px;
  height: 100%;
  width: 100%;
  opacity: 0;
  transition: opacity 300ms ease-in-out;
  flex-direction: column;

  &.entered {
    opacity: 1;
  }

  @media ${device.mobileM} {
    flex-direction: row;
    justify-content: space-evenly;
    align-self: stretch;
    align-items: center;
  }
`;

export interface SkillLayoutProps {
  transitionStatus: string;
}

const SkillsLayout: FC<SkillLayoutProps> = ({
  transitionStatus,
}): JSX.Element => (
  <Container className={transitionStatus}>
    <Skill text="REACT" japaneseText="レアクト" percentage={97}>
      {(percentage) => (
        <SkillHover percentage={percentage}>
          <ReactLogo />
        </SkillHover>
      )}
    </Skill>
    <Skill text="REDUX" japaneseText="リーダックス" percentage={93}>
      {(percentage) => (
        <SkillHover percentage={percentage}>
          <ReduxLogo />
        </SkillHover>
      )}
    </Skill>
    <Skill text="TYPESCRIPT" japaneseText="タイプスクリプト" percentage={83}>
      {(percentage) => (
        <SkillHover percentage={percentage}>
          <TypeScriptLogo />
        </SkillHover>
      )}
    </Skill>
    <Skill text="DOCKER" japaneseText="ドカー" percentage={45}>
      {(percentage) => (
        <SkillHover percentage={percentage}>
          <DockerLogo />
        </SkillHover>
      )}
    </Skill>
    <Skill text="JEST" japaneseText="ジェスト" percentage={79}>
      {(percentage) => (
        <SkillHover percentage={percentage}>
          <JestLogo />
        </SkillHover>
      )}
    </Skill>
  </Container>
);

export default SkillsLayout;
