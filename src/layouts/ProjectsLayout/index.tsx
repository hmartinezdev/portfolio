import React, { FC } from 'react';
import ProjectItem from '../../components/ProjectItem';
import { Project } from '../../parsers/projects';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { Carousel } from 'react-responsive-carousel';
import SliderIndicator from '../../components/SliderIndicator';
import ArrowIndicator from '../../components/ArrowIndicator';
import styled from 'styled-components';

export interface ProjectsLayoutProps {
  data: Project[];
}

const Container = styled.div`
  width: 100%;
  height: 100%;

  .slide.slide.slide {
    background-color: initial;
    height: 100%;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .slider-wrapper {
    height: 100%;
  }

  .axis-horizontal {
    height: 100%;
  }

  .slider {
    height: 100%;
  }

  .carousel-root {
    height: 100%;
  }

  .carousel {
    height: 100%;
  }

  .control-dots {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100px;
  }

  .carousel .slider-wrapper {
    overflow: initial;
  }
`;

const ArrowNextContainer = styled.div`
  position: absolute;
  right: 15px;
  bottom: 20px;
  z-index: 2;
`;

const ArrowPreviousContainer = styled.div`
  position: absolute;
  left: 15px;
  bottom: 20px;
  transform: rotate(180deg);
  z-index: 2;
`;

const ProjectsLayout: FC<ProjectsLayoutProps> = ({ data }) => {
  const renderIndicator = (
    onClickHandler: (e: React.MouseEvent<Element, MouseEvent>) => void,
    isSelected: boolean,
    index: number,
  ) => (
    <SliderIndicator
      onClickHandler={onClickHandler}
      isSelected={isSelected}
      type={data[index].type}
    />
  );

  const renderArrowNext = (clickHandler: () => void) => (
    <ArrowNextContainer>
      <ArrowIndicator onClick={clickHandler} />
    </ArrowNextContainer>
  );

  const renderArrowPrevious = (clickHandler: () => void) => (
    <ArrowPreviousContainer>
      <ArrowIndicator onClick={clickHandler} />
    </ArrowPreviousContainer>
  );

  const renderItem = (item, { isSelected }) => {
    return React.cloneElement(item, {
      isSelected,
    });
  };

  return (
    <Container>
      <Carousel
        showThumbs={false}
        showStatus={false}
        infiniteLoop={true}
        renderArrowNext={renderArrowNext}
        renderArrowPrev={renderArrowPrevious}
        renderIndicator={renderIndicator}
        useKeyboardArrows={true}
        transitionTime={300}
        renderItem={renderItem}>
        {data.map((value) => (
          <ProjectItem {...value} key={value.id} />
        ))}
      </Carousel>
    </Container>
  );
};

export default ProjectsLayout;
