/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React, { FC } from 'react';
import Header from '../../components/Header';
import './MainLayout.css';
import styled, { ThemeProvider } from 'styled-components';
import { defaultTheme } from '../../themes/default-theme';

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100vw;
  height: 100vh;
`;

export interface LayoutProps {
  children: JSX.Element;
  path: string;
}

const Layout: FC<LayoutProps> = ({ children }) => {
  return (
    <ThemeProvider theme={defaultTheme}>
      <Container>
        <Header />
        {children}
      </Container>
      <script type="text/javascript" src="/fonts/fonts.js" defer />
    </ThemeProvider>
  );
};

export default Layout;
