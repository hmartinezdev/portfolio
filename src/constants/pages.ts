export enum Pages {
  HOME = 'home',
  SKILLS = 'skills',
  PROJECTS = 'projects',
}

export const aliases = {
  ['/']: Pages.HOME,
  ['/skills/']: Pages.SKILLS,
  ['/projects/']: Pages.PROJECTS,
};
