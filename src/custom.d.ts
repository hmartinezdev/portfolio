declare module '*.svg';

declare interface ProjectsData {
  allMediumPost: {
    edges: [
      {
        node: {
          id: string;
          title: string;
          webCanonicalUrl: string;
          mediumUrl: string;
          virtuals: {
            subtitle: string;
            totalClapCount: number;
            previewImage: {
              imageId: string;
            };
          };
          uniqueSlug: string;
          author: {
            name: string;
          };
        };
      },
    ];
  };
  allGitlabProjects: {
    edges: [
      {
        node: {
          id: string;
          name: string;
          web_url: string;
          star_count: number;
          description: string;
        };
      },
    ];
  };
  githubData: {
    data: {
      user: {
        repositories: {
          edges: [
            {
              node: {
                id: string;
                name: string;
                url: string;
                description: string;
                stargazers: {
                  totalCount: number;
                };
              };
            },
          ];
        };
      };
    };
  };
}
