// import original module declarations
import 'styled-components';

// and extend them!
declare module 'styled-components' {
  export interface DefaultTheme {
    colors: {
      main: string;
      secondary: string;
      darkblue: string;
      blue: string;
      pink: string;
    };

    fonts: {
      primary: string;
      secondary: string;
    };
  }
}
