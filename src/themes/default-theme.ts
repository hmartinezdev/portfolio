import { DefaultTheme } from 'styled-components';

const defaultTheme: DefaultTheme = {
  colors: {
    main: '#fdf5f6',
    secondary: '#b8f2e6',
    darkblue: '#b9cdda',
    blue: '#a6d8d4',
    pink: '#fcebed',
  },
  fonts: {
    primary: `'Senko Hanabi', cursive`,
    secondary: `'Palanquin', sans-serif`,
  },
};

export { defaultTheme };
