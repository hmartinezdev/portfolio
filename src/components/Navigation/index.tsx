import React, { FC } from 'react';
import StyledLink from '../../styled/StyledLink';
import styled, { keyframes } from 'styled-components';
import { device } from '../../constants/mq';
import { useIntl } from 'gatsby-plugin-intl';

const opacity = keyframes`
  from {
    opacity: 0;
  }

  to {
    opactiy: 1;
  }
`;

const zoomin = keyframes`
  from {
    opacity: 0;
    transform: scale(1.2, 1.2) translateY(-40px);
  }

  to {
    opacity: 1;
    transform: scale(1, 1) translateY(0);
  }
`;

const StyledNavigation = styled.nav`
  position: absolute;
  display: flex;
  flex-direction: column;
  line-height: 10px;
  width: 100%;
  height: 100vh;
  padding-top: 60px;
  top: 0;
  left: 0;
  background: linear-gradient(
    180deg,
    rgba(0, 0, 0, 0.9) 85%,
    rgba(0, 0, 0, 0) 120%
  );
  animation: ${opacity} 150ms ease-in;
  overflow: hidden;

  &:nth {
    animation-delay: 100ms;
  }

  &:nth-of-type(3) {
    animation-delay: 100ms;
  }

  &.open {
    display: none;
    @media ${device.mobileL} {
      display: flex;
      opacity: 1;
    }
  }

  @media ${device.mobileL} {
    position: initial;
    flex-direction: row;
    line-height: 11px;
    margin-top: 6px;
    width: initial;
    height: initial;
    background: initial;
    padding-top: initial;
    animation: initial;
    overflow: initial;
  }

  @media ${device.tablet} {
    line-height: 14px;
  }
`;

const NavigationLink = styled(StyledLink)`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 30px;
  height: 150px;
  margin: 5px 10px;
  align-self: stretch;
  font-weight: 700;
  background-color: #b9cdda40;
  animation: ${zoomin} 150ms ease-in;
  border-left: 5px solid ${(props) => props.theme.colors.secondary}85;
  visibility: hidden;

  @media ${device.mobileL} {
    font-size: 14px;
    line-height: 11px;
    height: initial;
    margin: 0 10px;
    align-items: initial;
    animation: initial;
    border: initial;
    background: initial;
  }

  @media ${device.tablet} {
    font-size: 16px;
    line-height: 14px;
  }

  &.active {
    background: rgb(184, 242, 230);
    background: linear-gradient(
      90deg,
      rgba(184, 242, 230, 1) 0%,
      rgba(0, 0, 0, 0) 100%
    );
    border-left: 5px solid ${(props) => props.theme.colors.secondary};

    @media ${device.mobileL} {
      border: initial;
      color: ${(props) => props.theme.colors.secondary};
      background: initial;
    }
  }
`;

export interface NavigationProps {
  onClick: () => void;
  open: boolean;
}

export const Navigation: FC<NavigationProps> = ({ open, onClick }) => {
  const intl = useIntl();
  return (
    <StyledNavigation onClick={onClick} className={open ? 'open' : ''}>
      <NavigationLink to="/" activeClassName="active">
        {intl.formatMessage({ id: 'navigation.home' })}
      </NavigationLink>
      <NavigationLink to="/skills/" activeClassName="active">
        {intl.formatMessage({ id: 'navigation.skills' })}
      </NavigationLink>
      <NavigationLink to="/projects/" activeClassName="active">
        {intl.formatMessage({ id: 'navigation.projects' })}
      </NavigationLink>
    </StyledNavigation>
  );
};

export default Navigation;
