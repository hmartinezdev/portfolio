import React, { FC } from 'react';
import styled from 'styled-components';
import ArrowSvg from '../../assets/images/arrow.svg';
import { device } from '../../constants/mq';

const ArrowIcon = styled(ArrowSvg)`
  & circle:nth-child(2n) {
    fill: ${(props) => props.theme.colors.main};
  }

  & circle:nth-child(2n + 1) {
    fill: ${(props) => props.theme.colors.blue};
  }

  & circle:nth-child(3n + 2) {
    fill: ${(props) => props.theme.colors.secondary};
  }
`;

const ArrowContainer = styled.div`
  width: 50px;
  height: 50px;

  @media ${device.mobileL} {
    display: none;
  }
`;

export interface ArrowIndicatorProps {
  onClick: () => void;
}

const ArrowIndicator: FC<ArrowIndicatorProps> = ({ onClick }) => (
  <ArrowContainer>
    <ArrowIcon onClick={onClick} />
  </ArrowContainer>
);

export default ArrowIndicator;
