import React, { FC } from 'react';
import styled, { keyframes } from 'styled-components';
import { device } from '../../constants/mq';

const IconAnimation = keyframes`
    0% { opacity: 0; transform: translateY(-20%) scale(1.3, 1.3); }
    100% { opacity: 1; transform: translateY(0%) scale(1, 1); }
`;

const PercentageLeftAnimation = keyframes`
    0% { opacity: 0; transform: translateX(-20px); }
    100% { opacity: 1; transform: translateX(0); }
`;

const PercentageRightAnimation = keyframes`
    0% { opacity: 0; transform: translateX(20px); }
    100% { opacity: 1; transform: translateX(0); }
`;

const Container = styled.div`
  display: flex;
  flex-direction: row-reverse;
  justify-content: flex-start;
  align-items: center;
  height: 100%;

  @media ${device.mobileM} {
    flex-direction: column;
    align-items: center;
  }
`;

const Percentage = styled.div`
  font-family: ${(props) => props.theme.fonts.secondary};
  color: ${(props) => props.theme.colors.secondary};
  font-style: italic;
  display: flex;
  flex-direction: column;
  align-items: center;

  @media ${device.mobileM} {
    margin-top: 40px;
  }

  @media ${device.tablet} {
    display: block;
    flex-direction: none;
  }
`;

const Slash = styled.span`
  display: inline-block;
  position: relative;
  top: 3px;
  font-family: ${(props) => props.theme.fonts.secondary};
  color: ${(props) => props.theme.colors.pink};
  font-size: 700;
  margin: 0 5px;
  font-size: 25px;
  font-style: italic;
  display: none;

  @media ${device.tablet} {
    display: inline-block;
  }

  @media ${device.laptop} {
    font-size: 45px;
  }
`;

const HorizontalSlash = styled.hr`
  width: 20px;
  height: 3px;
  background-color: ${(props) => props.theme.colors.pink};
  margin: 0;
  margin-top: 8px;

  @media ${device.mobileM} {
    width: 24px;
  }

  @media ${device.tablet} {
    display: none;
  }
`;

const LeftPercentage = styled.span`
  display: inline-block;
  animation: ${PercentageLeftAnimation} 300ms ease-in;
  font-family: ${(props) => props.theme.fonts.secondary};
  font-size: 25px;
  font-style: italic;
  font-weight: 700;

  @media ${device.mobileM} {
    font-size: 30px;
  }

  @media ${device.laptop} {
    font-size: 45px;
  }
`;

const RightPercentage = styled.span`
  display: inline-block;
  animation: ${PercentageRightAnimation} 300ms ease-in;
  font-family: ${(props) => props.theme.fonts.secondary};
  font-size: 25px;
  font-style: italic;
  font-weight: 700;

  @media ${device.mobileM} {
    font-size: 30px;
  }

  @media ${device.laptop} {
    font-size: 45px;
  }
`;

const Logo = styled.span`
  & > svg {
    margin: 0 15px;
    width: 43px;
    height: 43px;
    animation: ${IconAnimation} 300ms ease-in;

    @media ${device.mobileM} {
      width: 48px;
      height: 48px;
      margin: 0 5px;
    }

    @media ${device.tablet} {
      width: 60px;
      height: 60px;
    }

    @media ${device.laptop} {
      width: 80px;
      height: 80px;
    }
  }
`;

export interface SkillHoverProps {
  children: JSX.Element;
  percentage: number;
}

const SkillHover: FC<SkillHoverProps> = ({ children, percentage = 50 }) => (
  <Container>
    <Logo>{children}</Logo>
    <Percentage>
      <LeftPercentage>{percentage}</LeftPercentage>
      <Slash>/</Slash>
      <HorizontalSlash />
      <RightPercentage>100</RightPercentage>
    </Percentage>
  </Container>
);

export default SkillHover;
