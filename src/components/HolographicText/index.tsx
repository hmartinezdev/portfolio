import React, { FC } from 'react';
import styled from 'styled-components';

interface HolographicProps {
  text: string;
}

export const Holographic = styled.span<HolographicProps>`
  display: block;
  font-family: ${(props) => props.theme.fonts.secondary};
  text-align: center;
  color: ${(props) => props.theme.colors.main};
  position: relative;
  font-weight: 400;
  opacity: 0.75;
  margin: 5px 0;
  font-weight: 400;

  &:after {
    content: '${(props) => props.text}';
    position: absolute;
    word-spacing: inherit;
    left: 2px;
    top: 1px;
    color: ${(props) => props.theme.colors.secondary};
    opacity: 0.65;
    width: 100%;
  }
`;

const HolographicText: FC<HolographicProps> = ({ text }) => (
  <Holographic text={text}>{text}</Holographic>
);

export default HolographicText;
