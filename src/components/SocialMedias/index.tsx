import React from 'react';
import styled from 'styled-components';
import Hexagon from '../../styled/Hexagon';
import Gitlab from './../../assets/images/gitlab.svg';
import StackOverflow from './../../assets/images/stack-overflow.svg';
import Medium from './../../assets/images/medium.svg';
import LinkedIn from './../../assets/images/linkedin.svg';
import { device } from '../../constants/mq';

const HexagonWithHover = styled(Hexagon)`
  display: flex;
  justify-content: center;
  align-items: center;
  &:hover {
    transform: scale(1.2, 1.2);
    cursor: pointer;

    & svg {
      transform: scale(1.3, 1.3);
    }
  }
`;
const GitlabIcon = styled(Gitlab)`
  height: 28px;
  width: 28px;

  @media ${device.mobileM} {
    width: 36px;
    height: 36px;
  }

  @media ${device.tablet} {
    width: 36px;
    height: 36px;
  }
`;

const StackOverflowIcon = styled(StackOverflow)`
  width: 26px;
  height: 26px;
  margin-bottom: 3px;

  @media ${device.mobileM} {
    width: 34px;
    height: 34px;
  }

  @media ${device.tablet} {
    width: 34px;
    height: 34px;
  }
`;

const MediumIcon = styled(Medium)`
  width: 26px;
  height: 26px;

  @media ${device.mobileM} {
    width: 34px;
    height: 34px;
  }

  @media ${device.tablet} {
    width: 34px;
    height: 34px;
  }
`;

const LinkedinIcon = styled(LinkedIn)`
  width: 24px;
  height: 24px;

  @media ${device.mobileM} {
    width: 32px;
    height: 32px;
  }

  @media ${device.tablet} {
    width: 32px;
    height: 32px;
  }
`;

const Container = styled.div`
  display: flex;
  justify-self: flex-end;
  margin-top: 20px;

  & > * {
    margin: 0 15px;
  }

  & a {
    font-size: 0;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;

const width = 40;
const tabletWidth = 52;
const desktopWidth = tabletWidth;

const SocialMedias = (): JSX.Element => (
  <Container>
    <HexagonWithHover
      mobileWidth={width}
      tabletWidth={tabletWidth}
      desktopWidth={desktopWidth}>
      <a
        href="https://gitlab.com/hmartinezdev"
        rel="noopener noreferrer"
        target="_blank">
        Gitlab
        <GitlabIcon />
      </a>
    </HexagonWithHover>
    <HexagonWithHover
      mobileWidth={width}
      tabletWidth={tabletWidth}
      desktopWidth={desktopWidth}>
      <a
        href="https://stackoverflow.com/users/6143767/hugo-martinez"
        rel="noopener noreferrer"
        target="_blank">
        Stack Overflow
        <StackOverflowIcon />
      </a>
    </HexagonWithHover>
    <HexagonWithHover
      mobileWidth={width}
      tabletWidth={tabletWidth}
      desktopWidth={desktopWidth}>
      <a
        href="https://medium.com/@hugo.83300"
        rel="noopener noreferrer"
        target="_blank">
        Medium
        <MediumIcon />
      </a>
    </HexagonWithHover>
    <HexagonWithHover
      mobileWidth={width}
      tabletWidth={tabletWidth}
      desktopWidth={desktopWidth}>
      <a
        href="https://www.linkedin.com/in/hugo-martinez-917772a7/"
        rel="noopener noreferrer"
        target="_blank">
        LinkedIn
        <LinkedinIcon />
      </a>
    </HexagonWithHover>
  </Container>
);

export default SocialMedias;
