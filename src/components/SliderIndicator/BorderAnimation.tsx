import styled from 'styled-components';

export interface BorderAnimationProps {
  active: boolean;
}

const BorderAnimation = styled.div<BorderAnimationProps>`
  position: relative;

  &:before {
    content: '';
    width: 130%;
    height: 130%;
    position: absolute;
    border: ${(props) => props.theme.colors.secondary} solid 2px;
    opacity: ${(props) => (props.active ? '1' : '0')};
    transition-duration: 0.3s;
    transition-property: transform opacity;
    transform: ${(props) => (props.active ? 'scale(1, 1)' : 'scale(1.3, 1.3)')};
    border-radius: 100%;
  }

  &:hover:before {
    opacity: 1;
    transform: scale(1, 1);
  }
`;

export default BorderAnimation;
