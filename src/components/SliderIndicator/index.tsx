import React, { FC } from 'react';
import { ProjectTypes } from '../../parsers/projects';
import styled from 'styled-components';
import ProjectIcon from '../../components/ProjectIcon';
import BorderAnimation from './BorderAnimation';
import { device } from '../../constants/mq';

const IndicatorContainer = styled.li`
  margin: 0 25px;
  display: none;
  &:hover {
    cursor: pointer;
  }

  & svg {
    width: 30px;
    height: 30px;
  }

  @media ${device.mobileL} {
    display: block;
  }
`;

const Indicator = styled(BorderAnimation)`
  position: relative;
  width: 55px;
  height: 55px;
  border-radius: 100%;
  background-color: ${(props) => props.theme.colors.darkblue}89;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export interface SliderIndicatorProps {
  onClickHandler: (e: React.MouseEvent<Element, MouseEvent>) => void;
  isSelected: boolean;
  type: ProjectTypes;
}

const SliderIndicator: FC<SliderIndicatorProps> = ({
  type,
  onClickHandler,
  isSelected,
}) => (
  <IndicatorContainer onClick={onClickHandler}>
    <Indicator active={isSelected}>
      <ProjectIcon type={type} />
    </Indicator>
  </IndicatorContainer>
);

export default SliderIndicator;
