import React, { FC } from 'react';
import styled from 'styled-components';
import { TransitionPortal } from 'gatsby-plugin-transition-link';
import './animation.css';

const animationDuration = '1500ms';
const dimension = '50px';

const ExitAnimation = styled.div`
  opacity: 1;
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  background-color: ${(props) => props.theme.colors.main};
  width: ${dimension};
  height: ${dimension};
  border-radius: 50%;
  z-index: 9999;
  animation: ${animationDuration} mainCircle ease-out;
  will-change: opacity, transform;
`;

const FirstCircle = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  border: 5px solid ${(props) => props.theme.colors.pink};
  width: calc(${dimension} + 20px);
  height: calc(${dimension} + 20px);
  border-radius: 50%;
  animation: ${animationDuration} firstCirlce ease-out;
  will-change: opacity, transform;
`;

const SecondCircle = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  border: 5px solid ${(props) => props.theme.colors.secondary};
  width: calc(${dimension} + 40px);
  height: calc(${dimension} + 40px);
  border-radius: 50%;
  animation: ${animationDuration} secondCircle ease-out;
  will-change: opacity, transform;
`;

const ThirdCircle = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  border: 5px solid ${(props) => props.theme.colors.blue};
  width: calc(${dimension} + 60px);
  height: calc(${dimension} + 60px);
  border-radius: 50%;
  animation: ${animationDuration} thirdCircle ease-out;
  will-change: opacity, transform;
`;

const FourthCircle = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  border: 5px solid ${(props) => props.theme.colors.darkblue};
  width: calc(${dimension} + 80px);
  height: calc(${dimension} + 80px);
  border-radius: 50%;
  animation: ${animationDuration} fourthCircle ease-out;
  will-change: opacity, transform;
`;

const Container = styled.div`
  position: absolute;
  width: 100vw;
  height: 100vh;
  display: none;

  & > div {
    animation-fill-mode: forwards;
  }

  &.exiting {
    display: block;
  }
`;

export interface TransitionAnimationProps {
  transitionStatus: string;
}

const TransitionAnimation: FC<TransitionAnimationProps> = ({
  transitionStatus,
}) => (
  <TransitionPortal level="top">
    <Container className={transitionStatus}>
      <ExitAnimation className={transitionStatus} />
      <FirstCircle className={transitionStatus} />
      <SecondCircle className={transitionStatus} />
      <ThirdCircle className={transitionStatus} />
      <FourthCircle className={transitionStatus} />
    </Container>
  </TransitionPortal>
);

export default TransitionAnimation;
