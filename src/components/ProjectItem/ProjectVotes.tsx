import React, { FC } from 'react';
import styled from 'styled-components';
import Star from '../../assets/images/star.svg';
import {
  mobileWidth,
  tabletWidth,
  desktopWidth,
} from './common/ProjectItem-constant';
import { device } from '../../constants/mq';
import {
  ProjectItemHexagon,
  ProjectItemContentContainer,
} from './common/ProjectItem-styled';

const StyledHexagon = styled(ProjectItemHexagon)`
  @media ${device.mobileM} {
    font-size: 50px;
  }

  & > svg {
    width: 80px;
    height: 80px;
  }
`;

const Vote = styled(ProjectItemContentContainer)`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Text = styled.p`
  position: relative;
  margin-right: 7px;
  font-size: 35px;
  font-family: ${(props) => props.theme.fonts.secondary};
  color: ${(props) => props.theme.colors.pink};
  bottom: 4px;

  @media ${device.mobileM} {
    font-size: 50px;
    bottom: 5px;
  }
  @media ${device.tablet} {
    font-size: 70px;
    bottom: 9px;
  }
`;

const StarIcon = styled(Star)`
  width: 30px;
  height: 30px;

  @media ${device.mobileM} {
    width: 42px;
    height: 42px;
  }
  @media ${device.tablet} {
    width: 60px;
    height: 60px;
  }
`;

export interface ProjectvotesProps {
  count: number;
}

const ProjectVotes: FC<ProjectvotesProps> = ({ count }) => (
  <StyledHexagon
    mobileWidth={mobileWidth}
    tabletWidth={tabletWidth}
    desktopWidth={desktopWidth}>
    <Vote>
      <Text>{count}</Text>
      <StarIcon />
    </Vote>
  </StyledHexagon>
);

export default ProjectVotes;
