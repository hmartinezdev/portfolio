import styled from 'styled-components';
import Hexagon from '../../../styled/Hexagon';
import { device } from '../../../constants/mq';

export const ProjectItemHexagon = styled(Hexagon)`
  transform: rotate(30deg);

  @media ${device.mobileL} {
    transform: initial;
  }
`;

export const ProjectItemContentContainer = styled.div`
  transform: rotate(-30deg);

  @media ${device.mobileL} {
    transform: initial;
  }
`;
