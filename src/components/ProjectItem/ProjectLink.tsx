import React, { FC } from 'react';
import styled from 'styled-components';
import {
  mobileWidth,
  tabletWidth,
  desktopWidth,
} from './common/ProjectItem-constant';
import { device } from '../../constants/mq';
import {
  ProjectItemHexagon,
  ProjectItemContentContainer,
} from './common/ProjectItem-styled';
import ProjectIcon from '../ProjectIcon';
import { ProjectTypes } from '../../parsers/projects';

const Link = styled.a`
  display: block;
  font-size: 0px;
  line-height: 0px;
  &:hover {
    cursor: pointer;

    & svg {
      transform: scale(1.3, 1.3);
    }
  }

  & svg {
    width: 40px;
    height: 40px;

    @media ${device.mobileM} {
      width: 60px;
      height: 60px;
    }
    @media ${device.tablet} {
      width: 80px;
      height: 80px;
    }
  }
`;

export interface ProjectLinkProps {
  type: ProjectTypes;
  url: string;
}

const ProjectLink: FC<ProjectLinkProps> = ({ type, url }) => (
  <Link href={url} rel="noopener noreferrer" target="_blank">
    {type}
    <ProjectItemHexagon
      mobileWidth={mobileWidth}
      tabletWidth={tabletWidth}
      desktopWidth={desktopWidth}>
      <ProjectItemContentContainer>
        <ProjectIcon type={type} />
      </ProjectItemContentContainer>
    </ProjectItemHexagon>
  </Link>
);

export default ProjectLink;
