import React, { FC } from 'react';
import styled from 'styled-components';
import {
  mobileWidth,
  tabletWidth,
  desktopWidth,
} from './common/ProjectItem-constant';
import { device } from '../../constants/mq';
import {
  ProjectItemContentContainer,
  ProjectItemHexagon,
} from './common/ProjectItem-styled';
import { defaultTheme } from '../../themes/default-theme';

const Description = styled(ProjectItemContentContainer)`
  font-size: 9px;
  font-family: ${(props) => props.theme.fonts.secondary};
  color: ${(props) => props.theme.colors.secondary};
  margin: 0;
  width: ${mobileWidth};
  display: inline-block;
  text-overflow: ellipsis;
  padding: 10px;
  font-weight: 700;

  @media ${device.mobileL} {
    font-size: 13px;
    width: ${tabletWidth};
  }

  @media ${device.tablet} {
    font-size: 17px;
    width: ${desktopWidth};
  }
`;

export interface ProjectDescriptionProps {
  description: string;
}

const ProjectDescription: FC<ProjectDescriptionProps> = ({ description }) => (
  <ProjectItemHexagon
    mobileWidth={mobileWidth}
    tabletWidth={tabletWidth}
    desktopWidth={desktopWidth}
    backgroundColor={`${defaultTheme.colors.main}40`}>
    <Description lang="en">
      <p>
        {description.length > 70
          ? `${description.substr(0, 70)}...`
          : description}
      </p>
    </Description>
  </ProjectItemHexagon>
);

export default ProjectDescription;
