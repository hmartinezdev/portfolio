import React, { FC } from 'react';
import Hexagon from '../../styled/Hexagon';
import styled from 'styled-components';
import { Project, ProjectTypes } from '../../parsers/projects';
import ProjectVotes from './ProjectVotes';
import ProjectTitle from './ProjectTitle';
import ProjectDescription from './ProjectDescription';
import {
  mobileWidth,
  tabletWidth,
  desktopWidth,
} from './common/ProjectItem-constant';
import { device } from '../../constants/mq';
import ProjectLink from './ProjectLink';

interface TypeProps {
  type: ProjectTypes;
}

const Container = styled.div<TypeProps>`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  position: relative;
  ${(props) =>
    props.type === ProjectTypes.MEDIUM &&
    `
    flex-direction: row-reverse;
  `}
  transform: translate(20px, -13%);

  @media ${device.mobileL} {
    flex-direction: column;
    font-size: 14px;
    width: ${tabletWidth};
    ${(props) =>
      props.type === ProjectTypes.MEDIUM &&
      `
    flex-direction: column-reverse;
  `}
    transform: translateY(-13%);
  }

  @media ${device.mobileL} {
    flex-direction: column;
    font-size: 14px;
    width: ${tabletWidth};
    ${(props) =>
      props.type === ProjectTypes.MEDIUM &&
      `
    flex-direction: column-reverse;
  `}
    transform: translateY(-13%);
  }
`;

const ItemsContainer = styled.div<TypeProps>`
  display: flex;
  justify-content: center;
  flex-direction: column;
  ${(props) =>
    props.type === ProjectTypes.MEDIUM &&
    `
    transform: translateX(-40px);
  `}

  @media ${device.mobileL} {
    flex-direction: row;
    transform: initial;
  }

  @media ${device.tablet} {
    flex-direction: row;
    transform: initial;
  }

  & > div {
    margin-left: 10px;
    margin-right: 10px;
  }
`;

const ItemsContainerBottom = styled(ItemsContainer)`
  transform: initial;
  transform: translateX(-20px);

  @media ${device.mobileL} {
    transform: translateY(-25px);
    ${(props) =>
      props.type === ProjectTypes.MEDIUM &&
      `
    transform: translateY(25px);
  `}
  }

  @media ${device.tablet} {
    transform: translateY(-47px);
    ${(props) =>
      props.type === ProjectTypes.MEDIUM &&
      `
    transform: translateY(47px);
  `}
  }
`;

const StyledHexagon = styled(Hexagon)`
  & > svg {
    width: 80px;
    height: 80px;
  }
`;

const HiddenElement = styled.div`
  visibility: hidden;
`;

export interface ProjectItemProps extends Project {
  isSelected?: boolean;
}

const Projectitem: FC<ProjectItemProps> = ({
  type,
  votes,
  name,
  description,
  url,
}) => {
  return (
    <Container type={type}>
      <ItemsContainer type={type}>
        <div>
          <ProjectLink type={type} url={url} />
        </div>
        <HiddenElement>
          <StyledHexagon
            mobileWidth={mobileWidth}
            tabletWidth={tabletWidth}
            desktopWidth={desktopWidth}></StyledHexagon>
        </HiddenElement>
        <div>
          <ProjectVotes count={votes} />
        </div>
      </ItemsContainer>
      <ItemsContainerBottom type={type}>
        <div>
          <ProjectTitle title={name} />
        </div>
        <div>
          <ProjectDescription description={description} />
        </div>
      </ItemsContainerBottom>
    </Container>
  );
};

export const ProjectItemHexagon = StyledHexagon;
export default Projectitem;
