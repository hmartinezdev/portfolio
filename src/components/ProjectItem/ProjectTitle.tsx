import React, { FC } from 'react';
import styled from 'styled-components';
import {
  mobileWidth,
  tabletWidth,
  desktopWidth,
} from './common/ProjectItem-constant';
import { device } from '../../constants/mq';
import {
  ProjectItemHexagon,
  ProjectItemContentContainer,
} from './common/ProjectItem-styled';
import HolographicText from '../HolographicText';

const Title = styled(ProjectItemContentContainer)`
  font-size: 12px;
  font-family: ${(props) => props.theme.fonts.secondary};
  color: ${(props) => props.theme.colors.pink};
  text-transform: uppercase;
  margin: 0;
  width: 200px;
  display: inline-block;
  text-overflow: ellipsis;
  font-weight: 700;

  @media ${device.mobileM} {
    font-size: 17px;
  }

  @media ${device.mobileL} {
    font-size: 19px;
  }

  @media ${device.tablet} {
    font-size: 26px;
  }
`;

export interface ProjectTitleProps {
  title: string;
}

const ProjectDescription: FC<ProjectTitleProps> = ({ title }) => (
  <ProjectItemHexagon
    mobileWidth={mobileWidth}
    tabletWidth={tabletWidth}
    desktopWidth={desktopWidth}>
    <Title>
      <HolographicText
        text={title.length > 30 ? `${title.substr(0, 30)}...` : title}
      />
    </Title>
  </ProjectItemHexagon>
);

export default ProjectDescription;
