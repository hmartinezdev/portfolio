import React, { FC, useState, useCallback } from 'react';
import styled from 'styled-components';
import ScrambleText from '../ScrambleText';
import {
  LatinDictionnary,
  KatakanaDictionnary,
} from '../../constants/dictionnary';
import { device } from '../../constants/mq';

const Container = styled.div`
  height: 100%;
  display: grid;
  width: 100%;
  position: relative;
`;

interface InfoContainerProps {
  percentage: number;
}

const HoverComponent = styled.div`
  position: absolute;
  top: 0;
  display: none;
  width: 100%;
  height: 100%;

  @media ${device.mobileM} {
    width: initial;
    height: initial;
  }
`;

const InfoContainer = styled.div<InfoContainerProps>`
  margin: 3px 0 3px 0;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  flex-direction: column;
  color: ${(props) => props.theme.colors.main};
  background: ${(props) =>
    `linear-gradient(-45deg, rgba(0, 0, 0, 0) ${
      80 - 80 * (props.percentage / 100)
    }%, rgb(184, 242, 230) 230%)`};
  position: relative;
  transition: all 200ms ease-in;

  &:hover {
    background: ${(props) =>
      `linear-gradient(-45deg, rgba(0, 0, 0, 0) ${
        80 - 80 * (props.percentage / 100)
      }%, rgb(184, 242, 230) 120%)`};
  }

  &:hover ${HoverComponent} {
    display: block;
  }

  @media ${device.mobileM} {
    margin: 5px 0 5px 5px;
    background: ${(props) =>
      `linear-gradient(180deg, rgba(0, 0, 0, 0) ${
        80 - 80 * (props.percentage / 100)
      }%, rgb(184, 242, 230) 230%)`};

    &:hover {
      background: ${(props) =>
        `linear-gradient(180deg, rgba(0, 0, 0, 0) ${
          80 - 80 * (props.percentage / 100)
        }%, rgb(184, 242, 230) 120%)`};
    }
  }
`;

const Text = styled.p`
  position: absolute;
  left: 10px;
  font-size: 18px;
  line-height: 80px;
  font-weight: 400;
  overflow: hidden;
  font-family: ${(props) => props.theme.fonts.secondary};
  margin: 0;
  font-weight: 700;

  @media ${device.mobileM} {
    font-size: 32px;
    writing-mode: vertical-lr;
    text-orientation: sideways;
    transform: rotate(180deg);
    bottom: 10px;
    left: initial;
  }

  @media ${device.tablet} {
    font-size: 35px;
  }
`;

export interface SkillProps {
  text: string;
  japaneseText: string;
  percentage: number;
  children: (percentage: number) => JSX.Element;
}

const Skill: FC<SkillProps> = ({
  text,
  japaneseText,
  percentage = 1,
  children,
}) => {
  const [textInfo, setTextInfo] = useState<{
    text: string;
    dictionnary: string;
    lang: string;
  }>({
    text: japaneseText,
    dictionnary: KatakanaDictionnary,
    lang: 'ja',
  });

  const onMouseOver = useCallback(() => {
    if (textInfo.text !== text) {
      setTextInfo({ text: text, dictionnary: LatinDictionnary, lang: 'en' });
    }
  }, []);

  return (
    <Container onMouseOver={onMouseOver}>
      <InfoContainer percentage={percentage}>
        <Text
          lang={textInfo.lang}
          className={textInfo.text === text && 'latintext'}>
          <ScrambleText
            text={textInfo.text}
            dictionnary={textInfo.dictionnary}
            appear={false}
            sequential
            interval={60}
          />
        </Text>
        <HoverComponent>{children(percentage)}</HoverComponent>
      </InfoContainer>
    </Container>
  );
};

export default Skill;
