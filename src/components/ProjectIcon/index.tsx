import React from 'react';
import GitlabIcon from './../../assets/images/gitlab.svg';
import GithubIcon from './../../assets/images/github.svg';
import MediumIcon from './../../assets/images/medium.svg';
import { ProjectTypes } from '../../parsers/projects';
import { FC } from 'react';

const projectIcons = {
  [ProjectTypes.GITLAB]: <GitlabIcon />,
  [ProjectTypes.GITHUB]: <GithubIcon />,
  [ProjectTypes.MEDIUM]: <MediumIcon />,
};

export interface ProjectIconProps {
  type: ProjectTypes;
}

const ProjectIcon: FC<ProjectIconProps> = ({ type }) => projectIcons[type];

export default ProjectIcon;
