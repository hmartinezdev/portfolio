/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React, { FC } from 'react';
import { Helmet } from 'react-helmet';
import { useIntl } from 'gatsby-plugin-intl';

export interface SEOProps {
  description?: string;
  lang?: string;
  title?: string;
}

const SEO: FC<SEOProps> = ({ description = '', title = '' }) => {
  const intl = useIntl();
  const defaultDescription = intl.formatMessage({ id: 'meta.description' });
  const mainTitle = intl.formatMessage({ id: 'meta.title' });
  const author = intl.formatMessage({ id: 'meta.author' });
  const lang = intl.formatMessage({ id: 'lang' });

  const metaDescription = description || defaultDescription;

  return (
    <Helmet
      htmlAttributes={{
        lang,
      }}>
      <title>{`${title ? `${title} | ` : ''}${mainTitle}`}</title>
      <meta name="description" content="Helmet application" />
      <html lang={lang} />
      <meta name="description" content={metaDescription} />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={metaDescription} />
      <meta property="og:type" content={'website'} />
      <meta name="twitter:card" content={'summary'} />
      <meta name="twitter:creator" content={author} />
      <meta name="twitter:description" content={metaDescription} />
      <meta name="twitter:title" content={title} />
    </Helmet>
  );
};

export default SEO;
