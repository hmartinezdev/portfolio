import React, { FC } from 'react';
import BackgroundImage from 'gatsby-background-image';
import styled from 'styled-components';
import { useStaticQuery, graphql } from 'gatsby';
import { Pages } from '../../constants/pages';

const StyledBackgroundSection = styled(BackgroundImage)`
  width: 100%;
  height: 100%;
  background-position: bottom center;
  background-repeat: repeat-y;
  background-size: cover;

  &::before,
  &::after {
    transition: background-image 500ms ease-in;
    filter: brightness(0.6);
  }
`;

export interface BackgroundProps {
  children: JSX.Element | JSX.Element[];
  path: Pages;
}

const Background: FC<BackgroundProps> = ({ children, path = Pages.HOME }) => {
  const bgData = useStaticQuery(graphql`
    query {
      home: file(relativePath: { eq: "home-bg.jpg" }) {
        childImageSharp {
          fluid(quality: 40, maxWidth: 1920) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
      skills: file(relativePath: { eq: "skills-bg.jpg" }) {
        childImageSharp {
          fluid(quality: 55, maxWidth: 1920) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
      projects: file(relativePath: { eq: "projects-bg.jpg" }) {
        childImageSharp {
          fluid(quality: 55, maxWidth: 1920) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
    }
  `);

  return (
    <StyledBackgroundSection
      Tag="section"
      fluid={bgData[path].childImageSharp.fluid}
      backgroundColor={`#040e18`}>
      {children}
    </StyledBackgroundSection>
  );
};

export default Background;
