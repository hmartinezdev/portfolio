import React, { FC, useState, useEffect, useCallback } from 'react';
import { KatakanaDictionnary } from '../../constants/dictionnary';
export interface ScrambleTextProps {
  text: string;
  sequential?: boolean;
  steps?: number;
  interval?: number;
  dictionnary?: string;
  startLength?: number;
  appear?: boolean;
}

const generateRandomString = (
  length: number,
  stringStart = '',
  dictionnary: string,
) => {
  let randomText = stringStart;
  while (randomText.length < length) {
    randomText += dictionnary.charAt(
      Math.floor(Math.random() * dictionnary.length),
    );
  }
  return randomText;
};

let timeout;

const ScrambleText: FC<ScrambleTextProps> = ({
  text,
  sequential = false,
  steps = 10,
  interval = 100,
  dictionnary = KatakanaDictionnary,
  startLength = text.length,
  appear = true,
}) => {
  const [allowAnimation, setAllowAnimation] = useState<boolean>(appear);
  const [displayedText, setDisplayedText] = useState<string>(
    generateRandomString(startLength, allowAnimation ? '' : text, dictionnary),
  );
  const animate = useCallback(
    (stepNumber, remainingSteps = stepNumber): number => {
      if (remainingSteps > 0) {
        const advancement = (remainingSteps / stepNumber) * text.length;
        const stringStart = sequential
          ? text.substr(0, text.length - Math.round(advancement))
          : '';
        let length = Math.trunc(text.length - advancement + startLength);
        length = length > text.length ? text.length : length;
        setDisplayedText(
          generateRandomString(length, stringStart, dictionnary),
        );
        timeout = setTimeout(
          animate.bind(this, stepNumber, remainingSteps - 1),
          interval,
        );
      } else {
        setDisplayedText(text);
        return -1;
      }
    },
    [text],
  );

  useEffect(() => {
    if (allowAnimation) {
      animate(steps);
    }
    setAllowAnimation(true);
    return () => {
      if (allowAnimation) {
        clearTimeout(timeout);
      }
    };
  }, [text]);

  return <>{displayedText}</>;
};

export default ScrambleText;
