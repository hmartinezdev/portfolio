import React, { useCallback, useState, FC } from 'react';
import Icon from './../../assets/images/logo.svg';
import {
  KatakanaDictionnary,
  LatinDictionnary,
} from '../../constants/dictionnary';
import ScrambleText from '../ScrambleText';
import styled from 'styled-components';
import { device } from '../../constants/mq';
import Navigation from '../Navigation';
import StyledLink from '../../styled/StyledLink';
import MobileMenuTrigger, { Cross } from '../MobileMenuTrigger';
import { useIntl } from 'gatsby-plugin-intl';

const CustomHeader = styled.div`
  display: flex;
  width: 100%;
  height: 60px;
  grid-area: header;
  align-items: center;
  justify-content: flex-start;
  padding-left: 10px;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 2;

  ${Cross} {
    margin: 0 12px 0 auto;

    @media ${device.mobileL} {
      display: none;
    }
  }
`;

const LogoText = styled.p`
  color: ${(props) => props.theme.colors.pink};
  font-family: ${(props) => props.theme.fonts.secondary};
  font-size: 18px;
  font-weight: bold;
  margin: 0 0 0 10px;
  letter-spacing: 6px;
  min-width: 100px;

  @media ${device.mobileM} {
    font-size: 23px;
    min-width: 120px;
  }

  @media ${device.tablet} {
    font-size: 28px;
    min-width: 140px;
  }

  &:first-letter {
    color: ${(props) => props.theme.colors.blue};
  }

  &:hover {
    cursor: pointer;
  }
`;

const LogoLink = styled(StyledLink)`
  display: flex;
  align-items: center;
`;

const Logo = styled(Icon)`
  height: 30px;
  min-width: 30px;
  width: 30px;

  @media ${device.mobileM} {
    height: 35px;
    min-width: 35px;
    width: 35px;
  }

  @media ${device.tablet} {
    height: 40px;
    min-width: 40px;
    width: 40px;
  }
`;

const Header: FC = () => {
  const japaneseText = 'レアクト';
  const normalText = 'REACT';
  const intl = useIntl();
  const [textInfo, setTextInfo] = useState<{
    text: string;
    dictionnary: string;
    lang: string;
  }>({ text: japaneseText, dictionnary: KatakanaDictionnary, lang: 'ja' });
  const [showMenu, setShowMenu] = useState<boolean>(false);

  const onMouseEnter = useCallback(() => {
    setTextInfo({
      text: normalText,
      dictionnary: LatinDictionnary,
      lang: intl.formatMessage({ id: 'lang' }),
    });
  }, []);

  const onClick = useCallback(() => {
    setShowMenu(!showMenu);
  }, [showMenu]);

  return (
    <CustomHeader>
      <LogoLink to="/">
        <Logo />
        <LogoText lang={textInfo.lang} onMouseEnter={onMouseEnter}>
          <ScrambleText
            sequential
            dictionnary={textInfo.dictionnary}
            text={textInfo.text}
            interval={75}
            appear={false}
          />
        </LogoText>
      </LogoLink>
      <Navigation onClick={onClick} open={!showMenu} />
      <MobileMenuTrigger onClick={onClick} open={showMenu} />
    </CustomHeader>
  );
};

export default Header;
