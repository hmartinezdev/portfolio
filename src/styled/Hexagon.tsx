import styled from 'styled-components';
import { device } from '../constants/mq';
import { defaultTheme } from '../themes/default-theme';

const widthRatio = 0.57736666666666666666666666667;
const marginRatio = 0.2887;
const defaultBackground = `${defaultTheme.colors.blue}40`;

export interface HexagonProps {
  mobileWidth: number;
  tabletWidth: number;
  desktopWidth: number;
  backgroundColor?: string;
}

const Hexagon = styled.div<HexagonProps>`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  width: ${(props) => props.mobileWidth}px;
  height: ${(props) => props.mobileWidth * widthRatio}px;
  background-color: ${(props) => props.backgroundColor || defaultBackground};
  margin: ${(props) => props.mobileWidth * marginRatio}px 0
    ${(props) => props.mobileWidth * marginRatio}px 0;

  @media ${device.mobileM} {
    width: ${(props) => props.tabletWidth}px;
    height: ${(props) => props.tabletWidth * widthRatio}px;
    margin: ${(props) => props.tabletWidth * marginRatio}px 0;
  }
  @media ${device.tablet} {
    width: ${(props) => props.desktopWidth}px;
    height: ${(props) => props.desktopWidth * widthRatio}px;
    margin: ${(props) => props.desktopWidth * marginRatio}px 0;
  }

  & svg {
    z-index: 2;
    -webkit-filter: drop-shadow(1px 1px 3px rgba(0, 0, 0, 0.4));
    filter: drop-shadow(1px 1px 3px rgba(0, 0, 0, 0.4));
    transition: all 300ms steps(3, end);
  }

  &:before,
  &:after {
    content: '';
    position: absolute;
    width: 0;
    border-left: ${(props) => props.mobileWidth / 2}px solid transparent;
    border-right: ${(props) => props.mobileWidth / 2}px solid transparent;

    @media ${device.mobileM} {
      border-left: ${(props) => props.tabletWidth / 2}px solid transparent;
      border-right: ${(props) => props.tabletWidth / 2}px solid transparent;
    }

    @media ${device.tablet} {
      border-left: ${(props) => props.desktopWidth / 2}px solid transparent;
      border-right: ${(props) => props.desktopWidth / 2}px solid transparent;
    }
  }

  &:before {
    bottom: 100%;
    border-bottom: ${(props) => props.mobileWidth * marginRatio}px solid
      ${(props) => props.backgroundColor || defaultBackground};

    @media ${device.mobileM} {
      border-bottom: ${(props) => props.tabletWidth * marginRatio}px solid
        ${(props) => props.backgroundColor || defaultBackground};
    }

    @media ${device.tablet} {
      border-bottom: ${(props) => props.desktopWidth * marginRatio}px solid
        ${(props) => props.backgroundColor || defaultBackground};
    }
  }

  &:after {
    top: 100%;
    width: 0;
    border-top: ${(props) => props.mobileWidth * marginRatio}px solid
      ${(props) => props.backgroundColor || defaultBackground};

    @media ${device.mobileM} {
      border-top: ${(props) => props.tabletWidth * marginRatio}px solid
        ${(props) => props.backgroundColor || defaultBackground};
    }

    @media ${device.tablet} {
      border-top: ${(props) => props.desktopWidth * marginRatio}px solid
        ${(props) => props.backgroundColor || defaultBackground};
    }
  }
`;

export default Hexagon;
