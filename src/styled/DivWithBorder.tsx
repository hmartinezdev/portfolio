import styled from 'styled-components';

const DivWithBorder = styled.div`
  position: relative;

  &:before {
    content: '';
    border: 3px solid transparent;
    border-image: ${(props) => `linear-gradient(
    -20deg,
    rgba(255, 255, 255, 0) 60%,
    ${props.theme.colors.pink}
  )`};
    border-image-slice: 1;
    position: absolute;
    width: 60%;
    height: 103%;
    left: 0;
    bottom: 0;
    z-index: 0;
  }

  &:after {
    content: '';
    border: 3px solid transparent;
    border-image: ${(props) => `linear-gradient(
    155deg,
    rgba(255, 255, 255, 0) 60%,
    ${props.theme.colors.pink}
  )`};
    border-image-slice: 1;
    position: absolute;
    width: 60%;
    height: 103%;
    right: 0;
    bottom: 0;
    z-index: 0;
  }
`;

export default DivWithBorder;
