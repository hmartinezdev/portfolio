import styled from 'styled-components';

const StyledLink = styled.a`
  color: ${(props) => props.theme.colors.secondary};
  font-family: ${(props) => props.theme.fonts.secondary};
  text-decoration: none;
  font-size: inherit;

  &:hover {
    color: #90fff6;
    cursor: pointer;
  }
`;

export default StyledLink;
