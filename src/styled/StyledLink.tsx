import { Link } from 'gatsby-plugin-intl';
import styled from 'styled-components';

const StyledLink = styled(Link)`
  color: ${(props) => props.theme.colors.pink};
  font-family: ${(props) => props.theme.fonts.secondary};
  font-size: 18px;
  text-decoration: none;
  margin: none;

  &:hover {
    color: ${(props) => props.theme.colors.blue};
  }
`;

export default StyledLink;
