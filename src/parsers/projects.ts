export enum ProjectTypes {
  GITLAB = 'GITLAB',
  MEDIUM = 'MEDIUM',
  GITHUB = 'GITHUB',
}

export interface Project {
  id: string;
  name: string;
  url: string;
  type: ProjectTypes;
  votes: number;
  description: string;
}

export default function parser(data: ProjectsData): Project[] {
  const GitlabProjects: Project[] = data.allGitlabProjects.edges.map<Project>(
    (value) => ({
      id: value.node.id,
      name: value.node.name,
      url: value.node.web_url,
      description: value.node.description,
      votes: value.node.star_count,
      type: ProjectTypes.GITLAB,
    }),
  );

  const MediumProjects: Project[] = data.allMediumPost.edges.map<Project>(
    (value) => ({
      id: value.node.id,
      name: value.node.title,
      votes: value.node.virtuals.totalClapCount,
      description: value.node.virtuals.subtitle,
      url: `https://medium.com/p/${value.node.uniqueSlug}`,
      type: ProjectTypes.MEDIUM,
    }),
  );

  const GithubProjects: Project[] = data.githubData.data.user.repositories.edges.map<
    Project
  >((value) => ({
    id: value.node.id,
    name: value.node.name,
    votes: value.node.stargazers.totalCount,
    description: value.node.description,
    url: value.node.url,
    type: ProjectTypes.GITHUB,
  }));

  return [...GitlabProjects, ...MediumProjects, ...GithubProjects];
}
