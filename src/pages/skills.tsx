import React, { FC } from 'react';
import styled from 'styled-components';
import Background from '../components/Background';
import { Pages } from '../constants/pages';
import SkillsLayout from '../layouts/SkillsLayout';
import SEO from '../components/SEO';
import { useIntl } from 'gatsby-plugin-intl';

const SkillsContainer = styled.main`
  height: 100%;
  width: 100%;
  position: relative;
  opacity: 1;
  transition: opacity 300ms ease-in-out;

  &.entering {
    opacity: 0;
  }

  &.exiting {
    opacity: 1;
  }
`;

const Title = styled.h1`
  display: none;
`;

const GlobalContainer = styled.div`
  width: 100%;
  height: 100%;
  opacity: 1;

  &.entering {
    opacity: 0;
  }
`;

export interface InfosPageProps {
  transitionStatus: string;
}

const InfosPage: FC<InfosPageProps> = ({ transitionStatus }) => {
  const intl = useIntl();
  return (
    <GlobalContainer className={transitionStatus}>
      <Background path={Pages.SKILLS}>
        <SkillsContainer className={transitionStatus}>
          <SkillsLayout transitionStatus={transitionStatus} />
          <SEO
            description={intl.formatMessage({
              id: 'skills.meta.description',
            })}
            title={intl.formatMessage({ id: 'skills.meta.title' })}
          />
          <Title>Skills</Title>
        </SkillsContainer>
      </Background>
    </GlobalContainer>
  );
};

export default InfosPage;
