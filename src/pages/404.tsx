import React from 'react';
import styled, { ThemeProvider } from 'styled-components';
import { defaultTheme } from '../themes/default-theme';
import Layout from '../layouts/404Layout';
import Background from '../components/Background';
import { Pages } from '../constants/pages';

const GlobalContainer = styled.div`
  width: 100%;
  height: 100%;
  opacity: 1;
  &.entering {
    opacity: 0;
  }
`;

const NotFoundPage = (): JSX.Element => (
  <ThemeProvider theme={defaultTheme}>
    <Background path={Pages.HOME}>
      <GlobalContainer>
        <Layout />
      </GlobalContainer>
    </Background>
  </ThemeProvider>
);

export default NotFoundPage;
