import React, { FC } from 'react';
import SEO from '../components/SEO';
import styled from 'styled-components';
import Background from '../components/Background';
import { Pages } from '../constants/pages';
import HomeLayout from '../layouts/HomeLayout';

const GlobalContainer = styled.div`
  width: 100%;
  height: 100%;
  opacity: 1;

  &.entering {
    opacity: 0;
  }
`;

export interface HomePageProps {
  transitionStatus: string;
}

const HomePage: FC<HomePageProps> = ({ transitionStatus }) => (
  <GlobalContainer className={transitionStatus}>
    <Background path={Pages.HOME}>
      <HomeLayout transitionStatus={transitionStatus} />
    </Background>
    <SEO />
  </GlobalContainer>
);

export default HomePage;
