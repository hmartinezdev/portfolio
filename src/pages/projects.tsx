import React, { FC } from 'react';
import styled from 'styled-components';
import Background from '../components/Background';
import { Pages } from '../constants/pages';
import SEO from '../components/SEO';
import ProjectsLayout from '../layouts/ProjectsLayout';
import { graphql } from 'gatsby';
import parser from '../parsers/projects';
import { useIntl } from 'gatsby-plugin-intl';

// GraphQl query
export const query = graphql`
  query onGithub {
    githubData {
      data {
        user {
          repositories {
            edges {
              node {
                id
                name
                url
                description
                stargazers {
                  totalCount
                }
              }
            }
          }
        }
      }
    }
    allGitlabProjects {
      edges {
        node {
          id
          name
          web_url
          star_count
          description
        }
      }
    }
    allMediumPost(sort: { fields: [createdAt], order: DESC }) {
      edges {
        node {
          id
          title
          virtuals {
            subtitle
            totalClapCount
            previewImage {
              imageId
            }
          }
          uniqueSlug
          author {
            name
          }
        }
      }
    }
  }
`;

const ProjectsContainer = styled.main`
  height: 100%;
  width: 100%;
  position: relative;
  opacity: 1;
  transition: opacity 300ms ease-in-out;
  padding-top: 60px;

  &.entering {
    opacity: 0;
  }

  &.exiting {
    opacity: 1;
  }
`;

const Title = styled.h1`
  display: none;
`;

const GlobalContainer = styled.div`
  width: 100%;
  height: 100%;
  opacity: 1;
  &.entering {
    opacity: 0;
  }
`;

export interface InfosPageProps {
  transitionStatus: string;
  data: ProjectsData;
}

const InfosPage: FC<InfosPageProps> = ({ data, transitionStatus }) => {
  const intl = useIntl();
  return (
    <GlobalContainer className={transitionStatus}>
      <Background path={Pages.PROJECTS}>
        <ProjectsContainer className={transitionStatus}>
          <Title>Projects</Title>

          <ProjectsLayout data={parser(data)} />
        </ProjectsContainer>
      </Background>
      <SEO
        description={intl.formatMessage({ id: 'projects.meta.description' })}
        title={intl.formatMessage({ id: 'projects.meta.title' })}
      />
    </GlobalContainer>
  );
};

export default InfosPage;
