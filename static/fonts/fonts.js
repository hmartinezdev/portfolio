
function fontLoad() {
  const fontSenkoHanabi = new FontFace(
    "Senko Hanabi",
    "url(/fonts/SenkoHanabiDEMO-Regular.woff)",
    {
      style: "normal",
      weight: "400"
    }
  );

  const fontPalanquin = new FontFace(
    "Palanquin",
    "url(/fonts/Palanquin-Regular.woff)",
    {
      style: "normal",
      weight: "400"
    }
  );

  const fontPalanquinBold = new FontFace(
    "Palanquin",
    "url(/fonts/Palanquin-Bold.woff)",
    {
      style: "normal",
      weight: "700"
    }
  );

  fontPalanquin.load(); // don't wait for render tree, initiate immediate fetch!
  fontPalanquinBold.load();
  fontSenkoHanabi.load();

  Promise.all([
    fontPalanquin.loaded,
    fontPalanquin.loaded,
    fontSenkoHanabi.loaded
  ]).then(function () {
    // apply the font (which may rerender text and cause a page reflow)
    // once the font has finished downloading
    document.fonts.add(fontSenkoHanabi);
    document.fonts.add(fontPalanquin);
    document.fonts.add(fontPalanquinBold);

    // OR... by default content is hidden, and rendered once font is available
    const content = document.getElementsByTagName("html");
    content[0].classList.add("wf-active");
  });
};

fontLoad();
